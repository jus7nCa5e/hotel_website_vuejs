<?php 
        $urlParamsRel = explode('/', $_SERVER['REQUEST_URI']);
        unset($urlParamsRel[1]);
        $urlParamsRelFin = implode('/', $urlParamsRel);
    ?>

    
<!DOCTYPE html>
<html lang>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta itemprop="name">
    <meta property="og:title">
    <meta name="twitter:title">
    <meta name="description"/>
    <meta itemprop="description">
    <meta property="og:description">
    <meta name="twitter:description">
    <meta name="keywords">
    <meta itemprop="image">
    <meta property="og:image">
    <meta name="twitter:image">
    <meta property="og:locale">
    <meta property="og:url">
    <meta name="twitter:url">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/fontawesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/regular.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/brands.css">
    <link rel="stylesheet" href="  https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/solid.min.css">
    <script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <link rel="icon" href="<%= BASE_URL %>favicon.ico">
    <link rel="alternate" hreflang="en" href="https://yastrebets.com/en<?php echo $urlParamsRelFin;?>" />
    <link rel="alternate" hreflang="bg" href="https://yastrebets.com/bg<?php echo $urlParamsRelFin;?>" />
    <title>Hotel Yastrebets Wellness & Spa - relaxing holiday at any time of the year in the mountains near Sofia. Perfect for ski holidays and spa retreats.</title>
  </head>
  <body>
    <noscript>
      <strong>We're sorry but Yastrebets Hotel Wellness and SPA doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app"></div>
    <!-- built files will be auto injected -->
  </body>
</html>


