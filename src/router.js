import { valHooks } from "jquery";
import Vue from "vue";
import Router from "vue-router";
// import Header from "./components/Header";
// import Main from "./components/Main";
// import Footer from "./components/Footer.vue";
// import Activitiesinner from "./components/Activitiesinner.vue";
// import Offerinner from "./components/Offerinner.vue";
import Href from "./components/Href.vue";
import Hrefinner from "./components/Hrefinner.vue";
// import Booking from "./components/Booking.vue";




Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  base: '/',
  mode: "history",
  routes: [
    {
      path: "/:lang/",
      name: "mains",
      component: Href
    //   components: {
    //     header: Header,
    //     default: Main,
    //     footer: Footer
    //   },
    },
    {
      path: '/',
      redirect: '/bg/'
    },
    {
      path:'/:lang/:alias',
      name: "firstlevel",
      component: Href
    },
    {
      path: "/:lang/:aliasfirst/:alias",
      component: Hrefinner
    },
    {
      path: "/:lang/:aliasfirst/:alias/:aliastab",
      name: "offerinner",
      component: Hrefinner
    },
    // {
    //   path: "/:lang/booking",
    //   component: Hrefinner
    // },
    // {
    //   path: "/:lang/:aliasfirst/:alias/:aliastab",
    //   name: "offerinner",
    //   components: {
    //     default: Offerinner,
    //     footer: Footer
    //   },
    // },
  ],
  // scrollBehavior(to, from, savedPosition) {
  //   return { x: 0, y: 0 };
  // },
   scrollBehavior () {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 })
      }, 500)
    })
  },
});
