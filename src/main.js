import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import router from "./router";
import moment from 'moment';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './App.vue'
import VuePlayerPlugin from 'vue-youtube-iframe-api'
import VueSimpleSVG from 'vue-simple-svg'
import VueCarousel from 'vue-carousel';
import vueHeadful from 'vue-headful';


Vue.component('vue-headful', vueHeadful);
Vue.use(VueCarousel);
Vue.use(VueSimpleSVG);


  
Vue.config.productionTip = false
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD MMMM, YYYY')
  }
});



var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);


import VueYoutube from 'vue-youtube'
 
Vue.use(VueYoutube)

// simple way
Vue.use(VuePlayerPlugin)
// OR, with configuration
Vue.use(VuePlayerPlugin, {
  with: '', // optional, used to set global width on all futur instance
  height: '', // optional, used to set global width on all futur instance
  loadComponent: true // create the global player component <vytia-player></vytia-player>
})

Vue.use(BootstrapVue)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')