export let youtube

var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '210',
          width: '340',
          videoId: 'D77wXvwChtU',
        });
      }
      function setCurrentTime(slideNum) {
        var object = [ 60, 120 ];
        player.seekTo(object[slideNum]);
      }

      // Custom function to LOOP
      // Moves playhead back to 20 seconds
      function handleVideo() 
      {
        setTimeout(handleVideo, 5000);
        player.seekTo(20);
      }